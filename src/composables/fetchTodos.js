import { onMounted, reactive, toRefs } from 'vue';

export function fetchTodos(props) {
  console.log(props);
  const state = reactive({
    results: [],
  });

  const fetchTodosFunc = async () => {
    const response = await fetch(
      'https://jsonplaceholder.typicode.com/' + props
    );
    const data = await response.json();
    state.results = data;
  };

  /* every lifecycle hook we might wanna use is prepented with an "on" */
  onMounted(fetchTodosFunc);

  return {
    ...toRefs(state),
    fetchTodosFunc,
  };
}
