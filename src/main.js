import { createApp } from 'vue';
// RootComponent
import App from './App.vue';

/*
Detailed Explanation:
    https://github.com/vuejs/rfcs/blob/master/active-rfcs/0009-global-api-change.md
*/

/*
 in test suites we can accidentally pollute other test cases if we don't
 store the original global configs.
 vue-test-utils (createLocalVue) to avoid that
*/

/*
In Vue 3, APIs that globally mutate Vue's behavior are now moved to application instances created by the new createApp method.
In addition, their effects are now scoped to that specific application's instance:
*/

// const app = createApp(App);

// app.mount('#app');
// app.directive('directive', exampleDirective);

createApp(App).mount('#app');

/*
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import { i18n } from './i18n';
import './vee-validate';

Vue.config.productionTip = false;

// Affects both instances
Vue.mixin({ ... })

const app1 = new Vue({ el: '#app-1' })
const app2 = new Vue({ el: '#app-2' })

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app');

    We can chain methods now on the createApp function
    this will then just take effect in one instance of vue
*/
