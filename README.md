# learnvue3

## Updating CLI

```
npm update -g @vue/cli
```

### Check the version you have

```
vue --version
```

Your cli version should be > 4.x

### Create a new Vue 3 Project

This works like before:

```
vue create myproject
```

---

> If you want to create a vue3 project, you must select (when asked) Vue Version 3.x (Preview)
